package model;

public class Member {

    private Long id;

    private String wxOpenId;

    private String unionid;


    public static Member selectByReferralCode(String referralCode) {
        return new Member();
    }

    public static Member selectByWxUnionId(String unionid) {
        return new Member();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWxOpenId() {
        return wxOpenId;
    }

    public void setWxOpenId(String wxOpenId) {
        this.wxOpenId = wxOpenId;
    }

    public String getUnionid() {
        return unionid;
    }

    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }
}