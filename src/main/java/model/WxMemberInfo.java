package model;

import com.huimi.core.base.BaseHmDTO;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;


/**
 * 微信授权用户信息
 */
@Data
public class WxMemberInfo extends BaseHmDTO<Long> {


    public static final String REDIS_URL = "WeChat:login:memberInfo:";
    /**
     * 用户名
     */
    private String userName;

    /**
     * 性别 0未知 1男 2女
     */
    private Integer sex;

    /**
     * 微信授权openId
     */
    private String openId;

    /**
     * 邀请码
     */
    private String inviteId;

    /**
     * 是否关注
     */
    private Integer subscribe;

    /**
     * w微信授权 unionId
     */
    private String unionId;

    /**
     * 国家
     */
    String country;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 门店shopId
     */
    private Integer shopId;

    /**
     * 微信头像地址
     */
    private String headImgUrl;


    /**
     * 注册标识 0 未注册 1 已注册
     **/
    public enum Register_FLAG {

        /**
         * 0,否:no
         **/
        NO("否", 0),

        /**
         * 1,是:yes
         **/
        YES("是", 1);

        public final int code;
        public final String value;
        private static Map<Integer, String> map = new HashMap<>();

        Register_FLAG(String value, int code) {
            this.code = code;
            this.value = value;
        }

        public static String getValue(Integer code) {
            if (null == code) {
                return null;
            }
            for (Register_FLAG Register_FLAG : Register_FLAG.values()) {
                if (Register_FLAG.code == code) {
                    return Register_FLAG.value;
                }
            }
            return null;
        }

        public static Integer getCode(String value) {
            if (null == value || "".equals(value)) {
                return null;
            }
            for (Register_FLAG Register_FLAG : Register_FLAG.values()) {
                if (Register_FLAG.value.equals(value)) {
                    return Register_FLAG.code;
                }
            }
            return null;
        }

        public static Map<Integer, String> getEnumMap() {
            if (map.size() == 0) {
                for (Register_FLAG Register_FLAG : Register_FLAG.values()) {
                    map.put(Register_FLAG.code, Register_FLAG.value);
                }
            }
            return map;
        }
    }
}