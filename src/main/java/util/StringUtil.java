package util;

import com.xiaoleilu.hutool.util.StrUtil;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * String 工具类
 */
public class StringUtil {

    private StringUtil() {}

    /**
     * 字符串空处理，去除首尾空格 如果str为null，返回"",否则返回str
     */
    private static String isNull(String str) {
        if (str == null) {
            return "";
        }
        return str.trim();
    }

    /**
     * 将对象转为字符串
     */
    public static String objToStr(Object o) {
        if (o == null) {
            return "";
        }
        String str;
        if (o instanceof String) {
            str = (String) o;
        } else {
            str = o.toString();
        }
        return str.trim();
    }

    /**
     * 大写字母转成“_”+小写
     */
    public static String toUnderline(String str) {
        char[] charArr = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        sb.append(charArr[0]);
        for (int i = 1; i < charArr.length; i++) {
            if (charArr[i] >= 'A' && charArr[i] <= 'Z') {
                sb.append('_').append(charArr[i]);
            } else {
                sb.append(charArr[i]);
            }
        }
        return sb.toString().toLowerCase();
    }

    /**
     * 首字母大写
     */
    static String firstCharUpperCase(String s) {
        return s.substring(0, 1).toUpperCase() + s.substring(1);
    }


    public static String unicode2String(String unicode) {
        StringBuilder string = new StringBuilder();
        String[] hex = unicode.split("\\\\u");
        for (String aHex : hex) {
            try {
                // 汉字范围 \u4e00-\u9fa5 (中文)
                if (aHex.length() >= 4) {//取前四个，判断是否是汉字
                    String chinese = aHex.substring(0, 4);
                    try {
                        int chr = Integer.parseInt(chinese, 16);
                        boolean isChinese = isChinese((char) chr);
                        //转化成功，判断是否在  汉字范围内
                        if (isChinese) {//在汉字范围内
                            // 追加成string
                            string.append((char) chr);
                            //并且追加  后面的字符
                            String behindString = aHex.substring(4);
                            string.append(behindString);
                        } else {
                            string.append(aHex);
                        }
                    } catch (NumberFormatException e1) {
                        string.append(aHex);
                    }

                } else {
                    string.append(aHex);
                }
            } catch (NumberFormatException e) {
                string.append(aHex);
            }
        }

        return string.toString();
    }

    public static String string2Unicode(String string) {
        StringBuilder result = new StringBuilder();
        if (StringUtil.isBlank(string)) {
            return null;
        }

        for (int i = 0; i < string.length(); i++) {
            // 取出每一个字符
            char chr1 = string.charAt(i);
            if (chr1 >= 19968) {// 汉字范围 \u4e00-\u9fa5 (中文)
                // 转换为unicode
                result.append("\\u").append(Integer.toHexString(chr1));
            } else {
                result.append(string.charAt(i));
            }
        }
        return result.toString();
    }

    /**
     * 判断是否为中文字符
     */
    private static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        return ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS;
    }

    /**
     * String to Long
     */
    public static long toLong(String str) {
        if (StringUtil.isBlank(str))
            return 0L;
        long ret = 0;
        try {
            ret = Long.parseLong(str);
        } catch (NumberFormatException ignored) {
        }
        return ret;
    }

    /**
     * 检验手机号
     */
    public static boolean isPhone(String phone) {
        phone = isNull(phone);
        Pattern regex = Pattern.compile("^((13[0-9])|(15[^4,\\D])|(17[0-9])|(18[0-9]))\\d{8}$");
        Matcher matcher = regex.matcher(phone);
        return matcher.matches();
    }

    /*
     * 判断是否为整数
     * @param str 传入的字符串
     * @return 是整数返回true,否则返回false
     */

    public static boolean isInteger(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    /**
     * 密码格式校验
     */
    private static boolean isPwd(String pwd) {
        if (pwd.length() < 8 || pwd.length() > 16) {
            return false;
        }
        boolean b1 = Pattern.compile("[0-9]").matcher(pwd).find();
        boolean b2 = Pattern.compile("(?i)[a-zA-Z]").matcher(pwd).find();
        return b1 && b2;
    }

    /**
     * 判断字符串是否为数字
     */
    public static boolean isNumber(String str) {
        if (isBlank(str)) {
            return false;
        }
        Pattern regex = Pattern.compile("(-)?\\d*(.\\d*)?");
        Matcher matcher = regex.matcher(str);
        return matcher.matches();
    }

    public static void main(String[] age) {
        System.out.println(isPwd("121323qq"));
    }

    /**
     * 模板字符串替换
     *
     * @return String string = "<p>您好！</p>\n" +
     * "<p>您刚才在{$site_name}重置了密码，新密码为：{$new_password}。</p>\n" +
     * "<p>请尽快登录 <a href=\"{$site_url}\" target=\"_blank\">{$site_url}</a> 修改密码。</p>";
     * HashMap<String, Object> map = new HashMap<>();
     * map.put("site_name", "B2B2C JAVA版");
     * map.put("new_password", "1101101111110");
     * map.put("site_url", "http://www.shopnc.net");
     * String stringNew = sendMessageHelper.replace(string, map);
     * System.out.println(stringNew);
     */
    public static String replace(String string, Map<String, Object> map) {
        if (string == null || string.length() == 0) {
            return "";
        }
        for (String key : map.keySet()) {
            string = string.replace("{$" + key + "}", String.valueOf(map.get(key)));
        }
        return string;
    }

    /**
     * 检验是否非空字符串
     */
    public static boolean isNotBlank(Object obj) {
        if (obj == null) return false;
        if (obj instanceof String) return StrUtil.isNotBlank((String) obj);
        return StrUtil.isNotBlank(obj.toString());
    }

    /**
     * 检验是否为空或空字符串
     */
    public static boolean isBlank(String str) {
        return StringUtil.isNull(str).equals("");
    }

    public static boolean isNotBlank(String str) {
        return !isBlank(str);
    }

    public static boolean isBlank(Object o) {
        return StringUtil.isNull(o).equals("");
    }

    public static String isNull(Object o) {
        if (o == null) {
            return "";
        }
        String str;
        if (o instanceof String) {
            str = (String) o;
        } else {
            str = o.toString();
        }
        return str;
    }

    /**
     * 验证邮箱
     */
    public static boolean checkEmail(String email) {
        boolean flag;
        try {
            String check = "^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+";
            Pattern regex = Pattern.compile(check);
            Matcher matcher = regex.matcher(email);
            flag = matcher.matches();
        } catch (Exception e) {
            flag = false;
        }
        return flag;
    }

    /**
     * public int indexOf(int ch, int fromIndex)
     * 返回在此字符串中第一次出现指定字符处的索引，从指定的索引开始搜索
     */
    public static int appearNumber(String srcText, String findText) {
        int count = 0;
        int index = 0;
        while ((index = srcText.indexOf(findText, index)) != -1) {
            index = index + findText.length();
            count++;
        }
        return count;
    }

    /**
     * 将unicode编码转为 中文
     */
    public static String convert(String utfString) {
        StringBuilder sb = new StringBuilder();
        int i = -1;
        int pos = 0;

        while ((i = utfString.indexOf("\\u", pos)) != -1) {
            sb.append(utfString.substring(pos, i));
            if (i + 5 < utfString.length()) {
                pos = i + 6;
                sb.append((char) Integer.parseInt(utfString.substring(i + 2, i + 6), 16));
            }
        }
        return sb.toString();
    }

    public static String decodeUnicode(String theString) {
        char aChar;
        int len = theString.length();
        StringBuilder outBuffer = new StringBuilder(len);
        for (int x = 0; x < len; ) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    // Read the xxxx
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                            case '0':
                            case '1':
                            case '2':
                            case '3':
                            case '4':
                            case '5':
                            case '6':
                            case '7':
                            case '8':
                            case '9':
                                value = (value << 4) + aChar - '0';
                                break;
                            case 'a':
                            case 'b':
                            case 'c':
                            case 'd':
                            case 'e':
                            case 'f':
                                value = (value << 4) + 10 + aChar - 'a';
                                break;
                            case 'A':
                            case 'B':
                            case 'C':
                            case 'D':
                            case 'E':
                            case 'F':
                                value = (value << 4) + 10 + aChar - 'A';
                                break;
                            default:
                                throw new IllegalArgumentException(
                                        "Malformed   \\uxxxx   encoding.");
                        }

                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't')
                        aChar = '\t';
                    else if (aChar == 'r')
                        aChar = '\r';
                    else if (aChar == 'n')
                        aChar = '\n';
                    else if (aChar == 'f')
                        aChar = '\f';
                    outBuffer.append(aChar);
                }
            } else
                outBuffer.append(aChar);
        }
        return outBuffer.toString();
    }

    /**
     * 拼接字符串方法
     */
    public static String joinMapValue(Map<String, Object> map, char connector) {
        StringBuilder b = new StringBuilder();
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            b.append(entry.getKey());
            b.append('=');
            if (entry.getValue() != null) {
                b.append(entry.getValue());
            }
            b.append(connector);
        }
        return b.toString().substring(0, b.length() - 1);
    }

    /**
     * 解析 json字符   格式为 1:1;1:1; 类型的value值集字符
     */
    public static String getJsonAndMapValue(String string) {
        try {
            String[] ss = string.split(";");
            StringBuilder str = new StringBuilder();
            for (String s : ss) {
                String[] strings = s.split(":");
                str.append(strings[1]).append(";");
            }
            return str.toString();
        } catch (Exception e) {
            return "";
        }
    }

    public static String fillTemplate(String template, Map<String, Object> sendData) {
        // 模板中的'是非法字符，会导致无法提交，所以页面上用`代替
        template = template.replace('`', '\'');
        try {
            return FreemarkerUtil.renderTemplate(template, sendData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static final String SEPARATOR =",";

    public static Set<Long> toSet(String tagStr){
        Set<Long> tag=new HashSet<>();
        if(StrUtil.isEmpty(tagStr)){
            return tag;
        }
        String[] split = tagStr.split(SEPARATOR);
        for (String s : split) {
            try {
                tag.add(Long.valueOf(s));
            }catch (NumberFormatException ignore){}
        }
        return tag;
    }


    public static String fillTemplet(String template, Map<String, Object> sendData) {
        // 模板中的'是非法字符，会导致无法提交，所以页面上用`代替
        template = template.replace('`', '\'');
        try {
            return FreemarkerUtil.renderTemplate(template, sendData);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
