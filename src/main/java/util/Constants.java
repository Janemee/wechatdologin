package util;

import com.google.common.base.Charsets;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统常量
 *
 * @author Vector
 * @create 2017-06-12 15:06
 */
public interface Constants {

    /**
     * 常量手机类型
     */
    int MOBILE_TYPE = 1;

    /**
     * 常量邮箱类型
     */
    int EMAIL_TYPE = 2;

    /**
     * 常量0
     */
    Integer INT_ZERO = 0;

    /**
     * 常量1
     */
    Integer INT_ONE = 1;

    /**
     * 常量0
     */
    String STR_ZERO = "0";

    /**
     * 常量0
     */
    String STR_ONE = "1";

    /**
     * 表单token标识
     */
    String FORM_TOKEN = "form_token";

    /**
     * 后台权限查询标识-代理商
     */
    String AG = "ag";

    /**
     * 后台权限查询标识-用户
     */
    String USER = "user";

    /**
     * 找回密码 - 邮箱验证码
     */
    String BACK_PWD_EMAIL = "back-pwd-code-email";
    /**
     * 找回密码 -手机验证码
     */
    String BACK_PWD_MOBILE = "back-pwd-code-mobile";

    String MEMBER_TOEKN = "member_token:";

    String Admin_Token = "admin_token:";
    /**
     * 找回支付密码 -手机验证码
     */
    String BACK_PAY_PWD_MOBILE = "back-pay-pwd-code-mobile";
    /**
     * 注册 - 手机验证码
     */
    String REGIST_CODE_MOBILE = "regist-pwd-mobile";
    /**
     * 注册 - 邮箱验证码
     */
    String REGIST_CODE_EMAIL = "regist-pwd-email";

    /**
     * 提币 - 邮箱验证码
     */
    String MENTION_COIN_CODE_EMAIL = "mention_coin-code-email";
    /**
     * 提币 - 手机验证码
     */
    String MENTION_COIN_CODE_MOBILE = "mention_coin-code-mobile";


    /**
     * 绑定手机 -手机验证码
     */
    String BIND_MOBILE = "bind-code-mobile";

    /**
     * 修改绑定手机 - 旧手机验证码
     */
    String OLD_BIND_NEW_MOBILE = "old-bind-new-mobile-code";

    /**
     * 修改绑定手机 - 新手机验证码
     */
    String NEW_BIND_NEW_MOBILE = "new-bind-new-mobile-code";


    /**
     * 绑定邮箱 -邮箱验证码
     */
    String BIND_EMAIL = "bind-code-email";

    /**
     * 修改登录密码 -手机验证码
     */
    String MODIFY_PWD_MOBILE = "modify-pwd-code-mobile";

    /**
     * 修改登录密码 -邮箱验证码
     */
    String MODIFY_PWD_EMAIL = "modify-pwd-code-email";


    /**
     * 找回提币密码 -手机验证码
     */
    String BACK_MENTION_PWD_MOBILE = "back-mention-pwd-code-mobile";

    /**
     * 找回提币密码 -邮箱验证码
     */
    String BACK_MENTION_PWD_EMAIL = "back-mention-pwd-code-email";

    /**
     * 绑定谷歌验证码 -手机验证码
     */
    String BIND_GOOGLE_KEY_MOBILE = "bind-google-key-code-mobile";

    /**
     * 绑定谷歌验证码 -邮箱验证码
     */
    String BIND_GOOGLE_KEY_EMAIl = "bind-google-key-code-email";

    /**
     * 修改谷歌验证码 -手机验证码
     */
    String MODIFY_GOOGLE_KEY_MOBILE = "modify-google-key-code-mobile";

    /**
     * 修改谷歌验证码 -邮箱验证码
     */
    String MODIFY_GOOGLE_KEY_EMAIL = "modify-google-key-code-email";

    /**
     * 开启/关闭谷歌验证码 -手机验证码
     */
    String CLOSE_OR_OPEN_GOOGLE_AUTH_MOBILE = "close-or-open-google-auth-code-mobile";

    /**
     * 开启/关闭谷歌验证码 -邮箱验证码
     */
    String CLOSE_OR_OPEN_GOOGLE_AUTH_EMAIL = "close-or-open-google-auth-code-email";

    /**
     * 添加新银行卡 -预留手机验证码
     */
    String ADD_NEW_BANK_CARD = "add_new_bank_card";


    /**
     * 系统编码
     */
    Charset CHARSET = Charsets.UTF_8;

    /**
     * 标识：是/否、启用/禁用等
     */
    interface Flag {

        Integer YES = 1;

        Integer NO = 0;
    }

    /**
     * 操作类型
     */
    interface Operation {
        /**
         * 添加
         */
        String ADD = "add";
        /**
         * 更新
         */
        String UPDATE = "update";
        /**
         * 删除
         */
        String DELETE = "delete";
    }

    /**
     * 性别
     */
    interface Sex {
        /**
         * 男
         */
        Integer MALE = 1;
        /**
         * 女
         */
        Integer FEMALE = 0;
    }

    //升序
    //降序
    int ASC = 1;
    int DESC = 2;

    /**
     * 超级管理员的roleId
     */
    Long SUPER_ADMIN_ROLE_ID = 1L;

    /**
     * 最终的超级管理员ID，不可被删除
     */
    Long SUPER_ADMIN_USER_ID = 3L;
    /**
     * 前台用户登录失败次数缓存
     */
    String MEMBER_LOGIN_ERROR_TIMES = "member-login-error-times:";
    /**
     * 后台用户登录失败次数缓存
     */
    String USER_LOGIN_ERROR_TIMES = "user-login-error-times:";
    String PROJECT_NAME = "springboot-template";
    String VERSION = "1.0.0";
    /**
     * 前端用户图片验证码缓存前缀
     */
    String MEMBER_CAPTCHA = "member-captcha:";
    /**
     * 后台用户图片验证码缓存前缀
     */
    String ADMIN_USER_CAPTCHA = "manage-user-captcha:";

    String MEMBER_SMS_CODE = "member-sms-captcha:";
    //短信验证码发送时间前缀
    String MEMBER_SMS_CODE_TIME = "member-sms-code-time:";

    String CART_PREFFIX = "shopping-cart:";


    /**
     * 微信消息跳转路径
     */
    enum WECHAT_NOTICE_URL {
        //用户端
        ORDER_DETAIL_URL("订单详情跳转路径", "/orderDetail?id="),
        RECHARGE_URL("充值通知跳转路径(未确定)", ""),
        CASH_URL("提现通知跳转路径", "/withdrawRecord"),
        OUT_ORDER_DETAIL_URL("退/换货通知跳转路径", "/refundDetail?orderId="),
        VIP_PAY_URL("购买会员通知跳转路径", "/my"),
        OUT_ORDER_DETAIL_URL1("佣金到账通知跳转路径", "/my"),
        //骑手端
        QS_THE_SHIP("待配送", "https://qs.qccm.shop/"),
        QS_OK_THE_SHIP("已送达", "https://qs.qccm.shop/"),
        QS_THE_OUT_SHOP("待退货", "https://qs.qccm.shop/"),
        QS_THE_TRADE_SHOP("待换货", "https://qs.qccm.shop/"),
        QS_OK_OUT_OR_TRADE_ORDER("已退货/已关闭", "https://qs.qccm.shop/");

        public final String name;
        public final String value;
        private static Map<String, String> map = new HashMap<>();

        WECHAT_NOTICE_URL(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public static String getValue(String code) {
            if (null == code) {
                return null;
            }
            for (WECHAT_NOTICE_URL status : WECHAT_NOTICE_URL.values()) {
                if (status.name == code) {
                    return status.value;
                }
            }
            return null;
        }

        public static String getCode(String value) {
            if (null == value || "".equals(value)) {
                return null;
            }
            for (WECHAT_NOTICE_URL status : WECHAT_NOTICE_URL.values()) {
                if (status.value.equals(value)) {
                    return status.name;
                }
            }
            return null;
        }

        public static Map<String, String> getEnumMap() {
            if (map.size() == 0) {
                for (WECHAT_NOTICE_URL status : WECHAT_NOTICE_URL.values()) {
                    map.put(status.name, status.value);
                }
            }
            return map;
        }
    }

}
