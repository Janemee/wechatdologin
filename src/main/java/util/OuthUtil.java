package util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.kit.JsonKit;
import com.jfinal.weixin.sdk.api.*;
import model.Member;
import model.WxMemberInfo;


import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @ClassName Outh
 * @Description TODO
 * @Author jzm
 * @Date : 2020/7/14 14:12
 **/

public class OuthUtil {

    static String vuePath = "";

    /**
     * 拼装微信授权请求code 地址
     *
     * @return
     */
    public String getWeChatOauthCodeUrl() {
        //授权通过后需要跳转的页面 该地址应该是vue前端页面的地址，如
        String redirectUri = "回调路径";
        String APPID = "APPID";
        String pathUrl = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + APPID + "&redirect_uri=" + redirectUri + "&response_type=code&scope=snsapi_userinfo&state=1#wechat_redirect";
        //随机生成唯一标识
        String token = (RandomUtils.randomUUID());
        return pathUrl;
    }

    /**
     * 微信授权 获取微信openId   方式1 （前端获取微信授权嘛code 请求本地接口 获取openId）
     *
     * @param code 微信授权code
     * @return
     */
    public void getWeChatOpenId(String code) {
        HashMap<String, Object> data = new HashMap<>();
        //邀请码
        Integer subscribe = 0;

        //用户同意授权，获取code
        if (code != null) {
            String appId = "APPID";
            String secret = "SECRET";
            //通过code换取网页授权access_token
            SnsAccessToken snsAccessToken = SnsAccessTokenApi.getSnsAccessToken(appId, secret, code);
            String token = snsAccessToken.getAccessToken();
            String openId = snsAccessToken.getOpenid();
            //拉取用户信息(需scope为 snsapi_userinfo)
            ApiResult apiResult = SnsApi.getUserInfo(token, openId);
            System.out.println("getUserInfo:" + apiResult.getJson());
            if (apiResult.isSucceed()) {
                JSONObject jsonObject = JSON.parseObject(apiResult.getJson());
                String nickName = jsonObject.getString("nickname");
                //用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
                int sex = jsonObject.getIntValue("sex");
                //城市
                String city = jsonObject.getString("city");
                //省份
                String province = jsonObject.getString("province");
                //国家
                String country = jsonObject.getString("country");
                String headimgurl = jsonObject.getString("headimgurl");
                headimgurl = headimgurl.replace("http://", "https://");
                String unionid = jsonObject.getString("unionid");
                System.out.println(unionid);
                //获取用户信息判断是否关注
                ApiResult userInfo = UserApi.getUserInfo(openId);
                System.out.println(JsonKit.toJson("is subsribe>>" + userInfo));
                if (userInfo.isSucceed()) {
                    String userStr = userInfo.toString();
                    subscribe = JSON.parseObject(userStr).getIntValue("subscribe");
                    System.out.println("是否关注:" + subscribe);
                }
                //将授权信息存入缓存中
                WxMemberInfo wxMemberInfo = new WxMemberInfo();
                wxMemberInfo.setCity(city);
                wxMemberInfo.setOpenId(openId);
                wxMemberInfo.setUnionId(unionid);
                wxMemberInfo.setCountry(country);
                wxMemberInfo.setProvince(province);
                wxMemberInfo.setSex(sex);
                wxMemberInfo.setUserName(nickName);
                wxMemberInfo.setSubscribe(subscribe);
                wxMemberInfo.setHeadImgUrl(headimgurl);
                System.out.println("WxMemberInfo :" + wxMemberInfo);
                data.put("openId", openId);
                data.put("registerFlag", WxMemberInfo.Register_FLAG.NO.code);
                if (StringUtil.isNotBlank(openId)) {
                    //以获取到openId  做逻辑处理
                }
            }
        }
    }


    /**
     * 授权回调  方式 2 （根据获取微信授权码地址中的回调地址处理获取微信openId）
     *
     * @param code
     * @param state
     * @param request
     * @return
     */
    public String notifyGetOpenId(String code, String state, HttpServletRequest request) {
        int subscribe;
        //用户同意授权，获取code
        if (code != null) {
            String appId = ApiConfigKit.getApiConfig().getAppId();
            String secret = ApiConfigKit.getApiConfig().getAppSecret();
            //通过code换取网页授权access_token
            SnsAccessToken snsAccessToken = SnsAccessTokenApi.getSnsAccessToken(appId, secret, code);
            String token = snsAccessToken.getAccessToken();
            String openId = snsAccessToken.getOpenid();
            //拉取用户信息(需scope为 snsapi_userinfo)
            ApiResult apiResult = SnsApi.getUserInfo(token, openId);

            System.out.println("getUserInfo:" + apiResult.getJson());
            if (apiResult.isSucceed()) {
                JSONObject jsonObject = JSON.parseObject(apiResult.getJson());
                String nickName = jsonObject.getString("nickname");
                //用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
                int sex = jsonObject.getIntValue("sex");
                String city = jsonObject.getString("city");//城市
                String province = jsonObject.getString("province");//省份
                String country = jsonObject.getString("country");//国家
                String headimgurl = jsonObject.getString("headimgurl");
                headimgurl = headimgurl.replace("http://", "https://");
                String unionid = jsonObject.getString("unionid");
                System.out.println(unionid);
                //获取用户信息判断是否关注
                ApiResult userInfo = UserApi.getUserInfo(openId);
                System.out.println(JsonKit.toJson("is subsribe>>" + userInfo));
                if (userInfo.isSucceed()) {
                    String userStr = userInfo.toString();
                    subscribe = JSON.parseObject(userStr).getIntValue("subscribe");
                    System.out.println("是否关注:" + subscribe);
                }
                String ip = IPUtil.getLoginAddress(request);

                //两种情况  1.用户自主注册。点击微信图标自主授权注册。这时邀请码为0。
                //         2.用户通过扫码或者点击邀请链接进行注册，这时state值是以in开头，则进入手机号码注册流程。这时获取到了openid了已经。
                if (state.startsWith("in")) {
                    String[] arr = state.split("-");
                    if (arr.length != 2) {
                        return "邀请码异常";
                    }
                    String redirectUri = "跳转页页面地址";
                    return "redirect:" + redirectUri + "?referralCode=" + arr[1];
                } else if ("0,0".equals(state)) {
                    //通过state值获取邀请人信息  这里的state是邀请码
                    //判断用户是否存在
                    Member member1 = new Member();
                    if (null == member1) {
                        //保存用户信息
                        Member member = new Member();
                        //获取vue我的页面地址
                        String redirectUri = vuePath;
                        return "redirect:" + redirectUri + "?Web-Token=" + cacheLoginInfo(member) + "&mobile=" +
                                (StringUtil.isBlank(member.getWxOpenId()) ? "0" : "1");
                    } else {
                        //获取vue我的页面地址
                        String redirectUri = vuePath;
                        return "redirect:" + redirectUri + "?Web-Token=" + cacheLoginInfo(member1) + "&mobile=" +
                                (StringUtil.isBlank(member1.getWxOpenId()) ? "0" : "1");
                    }

                } else {
                    //保存用户信息
                    String[] arr = state.split(",");
                    String referralCode;
                    String shopId;
                    if (arr.length == 2) {
                        referralCode = arr[0];
                        shopId = arr[1];
                    } else {
                        referralCode = "0";
                        shopId = "0";
                    }
                    Member member1 = Member.selectByReferralCode(referralCode);
                    Long inviteId = 0L;
                    if (null != member1) {
                        inviteId = member1.getId();
                    }
                    Member member2 = Member.selectByWxUnionId(unionid);
                    if (null == member2) {
                        //注册
                        Member member =new  Member();
                        //获取vue我的页面地址
                        String redirectUri =vuePath;
                        return "redirect:" + redirectUri + "?Web-Token=" + cacheLoginInfo(member) + "&mobile=" +
                                (StringUtil.isBlank(member.getId()) ? "0" : "1");
                    } else {
                        //获取vue我的页面地址
                        String redirectUri = vuePath;
                        return "redirect:" + redirectUri + "?Web-Token=" + cacheLoginInfo(member2) + "&mobile=" +
                                (StringUtil.isBlank(member2.getId()) ? "0" : "1");
                    }

                }
            }
            return apiResult.getErrorMsg();
        } else {
            return "code is null";
        }
    }

    protected String cacheLoginInfo(Member member) {
        String md5ofStr = removePreviousLoginInfo(member);
        String token = md5ofStr + "-" + RandomUtils.randomCustomUUID();
        //登录有效期一天
        return token;
    }
    private String removePreviousLoginInfo(Member member) {
        String s = member.getId().toString() + DateUtil.dateStr2(DateUtil.getNowDate());
        String md5ofStr = MD5.getMD5ofStr(s);
        return md5ofStr;
    }
}
